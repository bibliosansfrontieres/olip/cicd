# Overview

The OLIP CICD makes an extensive use of includes, YAML templates and variables,
 in order to allow code reuse and customization.

## Definitions files

**Definition** files define jobs.

They are written as YAML templates, which are called at the Pipeline files level.
This allows to write a Pipeline file which picks the desired jobs only.

Definition files do NOT create any job in a CICD pipeline.

## Pipelines files

**Pipeline** files create jobs by extending Definitions.

Pipeline files do NOT include Definitions.

## Recipes files

A **Recipe** file only includes a set of jobs definitions and a set of
pipelines using these jobs.
The resulting merged YAML file is a complete CICD pipeline.

It is a ready-to-use Recipe: by importing a Recipe into your project,
you should need no further configuration in order to get a functional
CICD pipeline within your project.

## Variables

Any Definitions file declares a set of Variables.
They are usually separated in three groups:

* Global Variables defined at the Group level are commented out
  but repeated in the Definition file as a quick reference/reminder.
  You should never need to override these.
* Variables that you usually don't need to override.
  For instance, the `DOCKER_VERSION` value is set to `20.10.12` in order
  to have a consistent behaviour across all build ; yet overriding this
  variable is possible if you ever need to.
* Variables that you may want to override.
  For instance, `BUILD_ARCH_LIST` defines the target architectures:
  `linux/arm/v7,linux/386,linux/amd64,linux/arm64` but you may want
  to only build for `linux/amd64` in order to get a result as fast
  as possible for testing purposes.

Example from the `docker-buildx.definitions.yml` file:

```yaml
variables:
  # Generic variables - you usually don't need to override these
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_HOST: tcp://127.0.0.1:2376
  DOCKER_TLS_VERIFY: 1
  DOCKER_CERT_PATH: "${DOCKER_TLS_CERTDIR}/client"
  DOCKER_VERSION: 20.10.12
  BUILDX_VERSION: 0.5.1

  # Defined at the OLIP group level:
  # DOCKER_HUB_USER: olipcicd
  # DOCKER_HUB_PASSWORD: ********
  # DOCKER_HUB_REPOSITORY: "offlineinternet"

  # Variables that you may want to override
  # Platforms: linux/amd64, linux/arm64, linux/riscv64, linux/ppc64le, linux/386, linux/mips64le, linux/mips64, linux/arm/v7, linux/arm/v6, linux/s390x
  BUILD_ARCH_LIST: linux/arm/v7,linux/386,linux/amd64,linux/arm64
```
